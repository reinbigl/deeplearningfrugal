#!/usr/bin/env python

from PIL import Image,ImageDraw, ImageShow
import numpy as np
import matplotlib.pyplot as plt
import time, os, sys
from cellpose import utils, io, models
import tifffile
import networkx as nx

def decision_block(image):
    print("Cellpose will start for image {}".format("S35"))
    image.save("./S35.tif")
    image = io.imread("./S35.tif")
    model = models.CellposeModel(gpu=False, model_type='/home/mariereinbigler/.cellpose/models/CP_20220420_140301')

    channels = [[3,0]]

    t1=time.time()

    masks, flows, styles = model.eval(image, diameter=50, flow_threshold=0.5, resample=True, channels=channels)    

    t2 =  time.time()

    print("Segmentation done in {} seconds".format(t2-t1))

    # save results as png

    io.save_to_png(image, masks, flows, "out_filename")

    t3 =  time.time()

    #outlines = utils.outlines_list(masks)
    #io.outlines_to_text(out_filename, outlines)

    #t3 =  time.time()
    '''
    times = open(out_path + "/" + times_filename, "a")
    times.write("{};{};{};{};\n".format(out_filename, t2-t1, t3-t2, t3-t1))
    times.close()
    '''
    if np.all(masks==0):
        return 0, False
    else:
        count = len(np.unique(masks))
        #print(count)
        return count, True





def go_through_resolution_V2(image_path, image_name, scale_factor, res_level, max_res_level, tile, tile_width, tile_height, position_x, position_y):
    global global_count
    #print("res: ")
    #print(results)
    count, zoom_in = decision_block(tile)
    #print("count : " + str(count))
    #print("zoom_in : " + str(zoom_in))
    #print("zoom_in: "+ str(zoom_in))
    #print(results[1])
    #add node in the graph
    if zoom_in: 
        print("count zoom in:" + str(count))
        if scale_factor*res_level <= max_res_level:
            print("Scale_factor: " + str(scale_factor*res_level))
            mm = tifffile.memmap(image_path+ "/" + str(res_level*scale_factor) + "/" + image_name)
            #print(mm.shape)
            new_position_x = position_x*scale_factor
            new_position_y = position_y*scale_factor
            #print("My parent is at position: ")
            #print(new_position_x, new_position_x + scale_factor*tile_width, new_position_y, new_position_y + tile_height*scale_factor)
            for i in range(scale_factor):
                for j in range(scale_factor):
                    #print("I'm subtile "+ str(i) + " " + str(j))
                    #print("I'm at position:")
                    #print((new_position_x+i*tile_width,new_position_x+(i+1)*tile_width), (new_position_y+j*tile_height,new_position_y+(j+1)*tile_height))
                    tile = Image.fromarray(mm[new_position_x+i*tile_width:new_position_x+(i+1)*tile_width, new_position_y+j*tile_height:new_position_y+(j+1)*tile_height])           
                    #print(new_position_x +i*tile_width, new_position_x +(i+1)*tile_width )
                    #print(new_position_y+j*tile_height, new_position_y+(j+1)*tile_height )
                    go_through_resolution_V2(image_path, image_name, scale_factor, res_level*scale_factor, max_res_level, tile, tile.size[0], tile.size[1], new_position_x, new_position_y) 
                    #if results is None: print("It is None")
            '''                                       
            #print("End of loop")
            print(scale_factor*res_level)
            if scale_factor*res_level == max_res_level:
                #print("I'm out here")
                #print("count" + str(count))
                global_count+= count if count is not None else 0
                return 
            '''
        else:
            print("count without extra zoom:" + str(count))
            #print(0 if results[1] is None else "None" )
            #print(scale_factor*res_level)
            #print("I'm after")
            #print("count " +  str(count))
            print("count: " + str(count))
            global_count+= count #if count is not None else 0
            print("global_count " + str(global_count))
            return  #0 if results[1] is None else results[1]    
    else:
        #print(0 if results[1] is None else "None" )
        #print(results)
        #print("I'm at the end")
        print("count :" + str(count))
        global_count+= count #if count is not None else 0
        return #results #0 if results[1] is None else results[1]   


def build_graph_V2(image_path, image_name, scale_factor, max_res_level, tile_width, tile_height, func):
    global global_count
    #open image of a specific file at corresponding res_level
    mm = tifffile.memmap(image_path+ "/1/" + image_name)
    G = nx.grid_graph(((int(mm.shape[0]/tile_width)+2), int(mm.shape[1]/tile_height)+2), periodic=False)
    for i in range(int(mm.shape[0]/tile_width)+1):
        for j in range(int(mm.shape[1]/tile_height)+1):
            x_max = min((i+1)*tile_width, mm.shape[0])
            y_max = min((j+1)*tile_height, mm.shape[1])
            tile = mm[i*tile_width:x_max, j*tile_height:y_max]             
            tile = Image.fromarray(tile)
            #print("I'm tile "+ str(i) + " " + str(j))
            
            global_count = 0
            go_through_resolution_V2(image_path, image_name, scale_factor, 1, max_res_level, tile, tile.size[0], tile.size[1], i*tile_width , j*tile_height)
            
            #count += 0 if results is None else results[1]
            #print(results[1], count)
            print("Moving to the next tile")
            G.nodes[i,j]["count"] = global_count
            print("Global : " + str(G.nodes[i,j]["count"]))
    
    return graph



def main():
    global_count = 0
    graph = build_graph_V2("./Test_image", "S35L1-Scene-18-QUADc3.tif", 2, 8, 256, 256, decision_block)



if __name__ == "__main__":
    main()










