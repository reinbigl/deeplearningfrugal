#! /bin/bash

start=`date +%s`
env_py=/home/reinbigler/miniconda3/envs/cellpose2/bin/python
#env_py=python

if [ $# -ne "5" ]; then
    echo "usage: $0 in_directory_path tmp_path out_path use_gpu pattern"
    exit 1
fi

if [ -f times_$pattern.txt ]; then
    rm times_$pattern.txt
fi

#source /home/mariereinbigler/miniconda3/bin/activate cellpose1

in_path=$1
tmp_path=$2
out_path=$3
use_gpu=$4
pattern=$5

files=$(find $in_path -name "*$pattern*" | rev | cut -d'/' -f1 |rev)

for file in $files; do
    echo $file
    $env_py convertCzi2TifCzifile.py $in_path $file $tmp_path
    tmp_filename=$(echo $file | cut -d'.' -f1).tiff    #$(echo $file)  #$(echo $file | cut -d'.' -f1).tiff
    echo $tmp_filename
    $env_py runCellpose.py $tmp_path $tmp_filename $out_path $use_gpu times_PSOD.txt
    #echo -e "\n" >> times.txt
    rm $tmp_path/$tmp_filename
done


end=`date +%s`

runtime=$((end-start))
echo $runtime >> times_$pattern.txt


exit 0
