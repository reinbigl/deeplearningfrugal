from czifile import czi2tif, imread
import sys


if len(sys.argv) != 4 :
    print("Usage: {sys.argv[0]} in_path in_filename out_path")
    exit()

   
in_filename = sys.argv[2]
out_filename = in_filename.split(".")[0] + ".tiff"
in_path =  sys.argv[1]
out_path = sys.argv[3]

print(in_filename, in_path, out_path)

czi2tif(in_path+in_filename, out_path+out_filename)

print("{} conversion succeeded and is stored as {} at {}".format(in_filename, out_filename,out_path))
