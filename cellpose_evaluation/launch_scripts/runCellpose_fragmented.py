import time, os, sys
from cellpose import utils, io, models 

if len(sys.argv) != 6 :
    print(f"Usage: {sys.argv[0]} in_path in_filename out_path use_gpu times_file")
    exit()

in_filename = sys.argv[2]
#out_filename = in_filename.split(".")[0] + ".tiff"
out_filename=in_filename
in_path =  sys.argv[1]
out_path = sys.argv[3]
use_gpu = sys.argv[4]
times_filename=sys.argv[5]


print("Cellpose will start for image {}".format(in_filename))

model = models.CellposeModel(gpu=True, model_type='cyto')#model_type='/home/reinbigler/.cellpose/models/CP_20220420_140301') #model_type='cyto')#model_type='/home/reinbigler/.cellpose/models/CP_20220420_140301')

channels = [[3,0]]

img = io.imread(in_path + "/" + in_filename)

t1=time.time()

masks, flows, styles = model.eval(img, diameter=45, flow_threshold=0.5, resample=True, channels=channels)

t2 =  time.time()

print("Segmentation done in {} seconds".format(t2-t1))

# save results as png

io.save_masks(img, masks, flows, out_filename, savedir=out_path)



t3 =  time.time()

#outlines = utils.outlines_list(masks)
#io.outlines_to_text(out_filename, outlines)

#t3 =  time.time()

times = open(out_path + "/" + times_filename, "a")
times.write("{};{};{};{};\n".format(out_filename, t2-t1, t3-t2, t3-t1))
times.close()
