from PIL import Image
import sys

def transform_masks_to_black(PILimage, width, height, masks):
    pixels = PILimage.load()
    for i in range(width) :#every pixel:
        for j in range(height):
            if (pixels[i,j] in masks) :
                # change to black if in masks
                pixels[i,j] = 0  

def remove_stripe_masks(PILimage, axis, width, height, size):
    valid_axis = {'horizontal', 'vertical'}
    if axis not in valid_axis:
        raise ValueError("Wrong axis given: axis can be 'horizontal' or 'vertical'")
    masks_to_remove = set();
    masks_on_limit = set()
    if (axis == 'horizontal'):
        for l in range(width):
            if (PILimage.getpixel((l, min(size-1, height-1))) != 0):
                masks_on_limit.add(PILimage.getpixel((l, min(size-1, height-1))))
        for i in range(width) :#every pixel:
            for j in range(size):
                if PILimage.getpixel((i, j)) not in masks_on_limit and PILimage.getpixel((i, j)) != 0:
                    masks_to_remove.add(PILimage.getpixel((i, j)))
        
        
    else:
        for l in range(height):
            if (PILimage.getpixel((min(size-1, width-1), l)) != 0):
                masks_on_limit.add(PILimage.getpixel((min(size-1, width-1), l)))
        
        for i in range(size) :#every pixel:
            for j in range(height):
                if PILimage.getpixel((i, j)) not in masks_on_limit and PILimage.getpixel((i, j)) != 0 :
                    masks_to_remove.add(PILimage.getpixel((i, j)))
    
    transform_masks_to_black(PILimage, width, height, masks_to_remove)
    return




def remove_cross_border_masks(PILimage, axis, width, height):
    valid_axis = {'horizontal', 'vertical'}
    if axis not in valid_axis:
        raise ValueError("Wrong axis given: axis can be 'horizontal' or 'vertical'")
    masks_to_remove = set();
    for i in range(width):
        if (PILimage.getpixel((i, height-1))!= 0):
            masks_to_remove.add(PILimage.getpixel((i, height-1)))

    for j in range(height):
        if (PILimage.getpixel((width-1, j))!= 0):
            masks_to_remove.add(PILimage.getpixel((width-1, j)))

    transform_masks_to_black(PILimage, width, height, masks_to_remove)  
    return


if __name__ == '__main__':
    name = sys.argv[1]
    print(name)
    exit
    directory = sys.argv[2]
    chopsize = 512
    intersection=200
    PILimage = Image.open(directory + name)
    width, height = PILimage.size
    print(width, height)
    x,y = name.split('.')[-3][1:], name.split('.')[-2][1:].split('_')[0]
    x_box = int(x) + chopsize + intersection
    y_box = int(y) + chopsize + intersection
    remove_stripe_masks(PILimage, 'horizontal', width, height, intersection)
    remove_stripe_masks(PILimage, 'vertical', width, height, intersection)
    if x_box != width-1 :
        remove_cross_border_masks(PILimage, 'horizontal', width, height)
    if y_box != height-1 :
        remove_cross_border_masks(PILimage, 'vertical', width, height)
    
    PILimage.save(directory + name)
