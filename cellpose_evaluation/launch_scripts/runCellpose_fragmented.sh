#! /bin/bash

start=`date +%s`
env_py=/home/reinbigler/miniconda3/envs/cellpose2/bin/python
#env_py=python

if [ $# -ne "5" ]; then
    echo "usage: $0 in_directory_path tmp_path out_path use_gpu pattern"
    exit 1
fi

if [ -f times_$pattern.txt ]; then
    rm times_$pattern.txt
fi

#source /home/mariereinbigler/miniconda3/bin/activate cellpose1

in_path=$1
tmp_path=$2
out_path=$3
use_gpu=$4
pattern=$5

files=$(find $in_path -name "*$pattern*" | rev | cut -d'/' -f1 |rev)

for file in $files; do
    echo $file
    #$env_py convertCzi2TifCzifile.py $in_path $file $tmp_path
    #tmp_filename=$(echo $file | cut -d'.' -f1).tiff
    
    #echo $tmp_filename
    #$env_py fragment_image.py $tmp_filename $tmp_path
    #rm $tmp_path/$tmp_filename
    for chunk in $(ls $tmp_path); do
	echo $chunk
	$env_py runCellpose_fragmented.py $tmp_path $chunk $out_path $use_gpu times_PSOD.txt
	#$env_py remove_overlap.py $(echo $chunk | rev | cut -d'.' -f2-6 | rev)_cp_masks.png $out_path
    done
    #echo -e "\n" >> times.txt
    rm $tmp_path/*
done


end=`date +%s`

runtime=$((end-start))
echo $runtime >> times_$pattern.txt


exit 0
