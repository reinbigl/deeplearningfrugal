def myScripts = [
    '/home/mariereinbigler/QuPath/projects/scripts/import_masks.groovy',
    '/home/mariereinbigler/Téléchargements/wetransfer_data-zip_2022-01-07_2150/scripts/Filter_annotations.groovy',
    '/home/mariereinbigler/Téléchargements/wetransfer_data-zip_2022-01-07_2150/scripts/SetAnnotationClass.groovy',
    '/home/mariereinbigler/QuPath/projects/scripts/import_verite_terrain.groovy',
    '/home/mariereinbigler/Téléchargements/wetransfer_data-zip_2022-01-07_2150/scripts/SetDetectionClass.groovy',
    ].collect(s -> new File(s))

for (script in myScripts) {
    run(script)
}
