import qupath.lib.objects.PathAnnotationObject
import qupath.lib.objects.classes.PathClassFactory

import groovy.io.*


import qupath.lib.roi.ROIs
import qupath.lib.regions.ImagePlane


import ij.gui.Wand
import qupath.lib.objects.PathObjects
import ij.IJ
import ij.process.ColorProcessor
import qupath.imagej.processing.RoiLabeling
import qupath.imagej.tools.IJTools
import java.util.regex.Matcher
import java.util.regex.Pattern

import qupath.lib.roi.GeometryTools;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.index.strtree.STRtree;

/**
   Inspired from:
 * Create a region annotation with a fixed size in QuPath, based on the current viewer location.
 *
 * @author Pete Bankhead
 */


def directoryPath = '/home/mariereinbigler/These/CellposeArticle/final/fragmented/masks/'
println(directoryPath)

File folder = new File(directoryPath);
File[] listOfFiles = folder.listFiles();

currentImport = listOfFiles.findAll{ qupath.lib.common.GeneralTools.getNameWithoutExtension(it.getPath()).contains(GeneralTools.getNameWithoutExtension(getProjectEntry().getImageName())) &&  it.toString().contains("_masks.png")}

print(currentImport)

Pattern pattern = Pattern.compile("x=\\d{4},y=\\d{4}|x=\\d{3},y=\\d{4}|x=\\d{4},y=\\d{3}|x=\\d{3},y=\\d{3}|x=\\d{5},y=\\d{4}|x=\\d{4},y=\\d{5}|x=\\d{5},y=\\d{5}|x=\\d{5},y=\\d{3}|x=\\d{3},y=\\d{5}");

xpositions = []
ypositions = []

for (File elt: currentImport) {
    println(elt.getName())
    Matcher matcher = pattern.matcher(elt.getName());
    println(matcher.find())
    result = matcher.group()
    println(result)
    pos = result.split("\\,")
    print(pos)
    xpos = pos[0].split("\\=")[1]
    ypos = pos[1].split("\\=")[1]
    xpositions.add(xpos)
    ypositions.add(ypos)
    println(xpos)
    println(ypos)
}
println(xpositions)

println(ypositions)



clearAllObjects()
double downsample = 1 // TO CHANGE (if needed)
ImagePlane plane = ImagePlane.getDefaultPlane()
def imageData = getCurrentImageData()

def server = imageData.getServer()

height = server.getHeight()
width = server.getWidth()

/**

def List<PathObject> filterDetections(List<PathObject> rawDetections) {

        // Sort by size
        rawDetections.sort(Comparator.comparingDouble(o -> -1 * o.getROI().getArea()));

        // Create array of detections to keep & to skip
        var detections = new LinkedHashSet<PathObject>();
        var skippedDetections = new HashSet<PathObject>();
        int skipErrorCount = 0;

        // Create a spatial cache to find overlaps more quickly
        // (Because of later tests, we don't need to update envelopes even though geometries may be modified)
        Map<PathObject, Envelope> envelopes = new HashMap<>();
        var tree = new STRtree();
        for (var det : rawDetections) {
            var env = det.getROI().getGeometry().getEnvelopeInternal();
            envelopes.put(det, env);
            tree.insert(env, det);
        }

        for (var detection : rawDetections) {
            if (skippedDetections.contains(detection))
                continue;

            detections.add(detection);
            var envelope = envelopes.get(detection);

            @SuppressWarnings("unchecked")
            var overlaps = (List<PathObject>) tree.query(envelope);
            for (var nuc2 : overlaps) {
                if (nuc2 == detection || skippedDetections.contains(nuc2) || detections.contains(nuc2))
                    continue;

                // If we have an overlap, retain the larger object only
                try {
                    var env = envelopes.get(nuc2);
                    //iou
                    Geometry intersection = detection.getROI().getGeometry().intersection(nuc2.getROI().getGeometry());
                    Geometry union = detection.getROI().getGeometry().union(nuc2.getROI().getGeometry());
                    double iou = intersection.getArea() / union.getArea();

                    // Get the difference between the two. In case the result is smaller than half the area of the largest object, remove it
                    // Or if it exceeds the allowed IoU threshold
                    Geometry difference = nuc2.getROI().getGeometry().difference(detection.getROI().getGeometry());

                    if (envelope.intersects(env) && detection.getROI().getGeometry().intersects(nuc2.getROI().getGeometry())) {
                         if( iou > this.iouThreshold || difference.getArea() < detection.getROI().getGeometry().getArea() / 2.0 ) {
                             skippedDetections.add(nuc2);
                         }
                    }
                } catch (Exception e) {
                    skippedDetections.add(nuc2);
                    skipErrorCount++;
                }

            }
        }
        if (skipErrorCount > 0) {
            int skipCount = skippedDetections.size();
            logger.warn("Skipped {} nucleus detection(s) due to error in resolving overlaps ({}% of all skipped)",
                    skipErrorCount, GeneralTools.formatNumber(skipErrorCount * 100.0 / skipCount, 1));
        }
        return new ArrayList<>(detections);
    }


**/








def tile_size = 2000

lines = []

def i_x = 0
def i_y = 0
/**
i_x += tile_size 
i_y += tile_size 

while (i_x < width) {
    def roi = ROIs.createLineROI(i_x, 0, i_x, height, plane)
    i_x += tile_size 
    lines << PathObjects.createAnnotationObject(roi)
}

while (i_y < width) {
    def roi = ROIs.createLineROI(0, i_y, width, i_y, plane)
    i_y += tile_size 
    lines << PathObjects.createAnnotationObject(roi)
}


addObjects(lines)
*/
int index = 0



for (File file: currentImport) {

    path = file.getPath()

    def imp = IJ.openImage(path)
    
    
    int n = imp.getStatistics().max as int
    if (n == 0) {
        print 'No objects found!'
        index+=1
        continue
    }
    def ip = imp.getProcessor()
        
    if (ip instanceof ColorProcessor) {
        throw new IllegalArgumentException("RGB images are not supported!")
    }
    def roisIJ = RoiLabeling.labelsToConnectedROIs(ip, n)
    
    def rois = roisIJ.collect {
        if (it == null)
                return
            //return IJTools.convertToROI(it, xpositions[index], ypositions[index], downsample, plane);
            int x = new Integer(xpositions[index])
            int y = new Integer(ypositions[index]) 
            return IJTools.convertToROI(it, -x, -y, downsample, plane);
    }
    rois = rois.findAll{null != it}
        
        // Convert QuPath ROIs to objects
        def pathObjects = rois.collect {
            return PathObjects.createDetectionObject(it)
        }
        addObjects(pathObjects)
        def detections = getDetectionObjects()
        //detections = filterDetections(detections)
        def newAnnotations = detections.collect {
            return PathObjects.createAnnotationObject(it.getROI(), it.getPathClass())
        }
        index+=1
        
        
removeObjects(detections, true)
addObjects(newAnnotations)

}

println("Lines drawn")

