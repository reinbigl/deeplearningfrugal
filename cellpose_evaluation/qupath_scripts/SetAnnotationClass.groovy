
import static qupath.lib.gui.scripting.QPEx.*

def prediction = getPathClass('Prediction')

selected = getAnnotationObjects()
for (def annotation in selected){
    annotation.setPathClass(prediction)
}

