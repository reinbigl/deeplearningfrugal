import org.locationtech.jts.geom.Geometry
import qupath.lib.common.GeneralTools
import qupath.lib.objects.PathObject
import qupath.lib.objects.PathObjects
import qupath.lib.roi.GeometryTools
import qupath.lib.roi.ROIs
import qupath.lib.gui.measure.ObservableMeasurementTableData

import qupath.lib.objects.PathAnnotationObject
import qupath.lib.objects.classes.PathClassFactory
import qupath.lib.roi.RectangleROI
//import qupath.lib.scripting.QPEx

def imageData = QPEx.getCurrentImageData()
ob = new ObservableMeasurementTableData();
annotations = getAnnotationObjects()
print("annotations = "+annotations.size())

minX = annotations[0].getROI().getBoundsX()
minY = annotations[0].getROI().getBoundsY()
maxX = minX + annotations[0].getROI().getBoundsWidth()
maxY = minY + annotations[0].getROI().getBoundsHeight()

ob.setImageData(getCurrentImageData(),  annotations);
annotations.each {
    roi = it.getROI()
    //geom = roi.getGeometry()
    //print(geom)
    bx = roi.getBoundsX()
    by = roi.getBoundsY()
    bh = roi.getBoundsHeight()
    bw = roi.getBoundsWidth()
    
    if (bx < minX) {
        minX = bx
       }
       
    if (by < minY) {
        minY = by
       }
       
    if (bx+bw > maxX) {
        maxX = bx+bw
       }
       
    if (by + bh > maxY) {
        maxY = by + bh
       }    
       
}


def box = new RectangleROI(minX, minY, maxX-minX, maxY-minY)

def annotation = new PathAnnotationObject(box, PathClassFactory.getPathClass("None"))
imageData.getHierarchy().addPathObject(annotation, true)

print(minX)
print(minY)
print(maxX)
print(maxY)
  

  
  
    