
import qupath.lib.objects.PathAnnotationObject
import qupath.lib.objects.classes.PathClassFactory
import qupath.lib.roi.RectangleROI

import qupath.lib.objects.PathAnnotationObject
import qupath.lib.objects.classes.PathClassFactory

import groovy.io.*


import qupath.lib.roi.ROIs
import qupath.lib.regions.ImagePlane


import ij.gui.Wand
import qupath.lib.objects.PathObjects
import ij.IJ
import ij.process.ColorProcessor
import qupath.imagej.processing.RoiLabeling
import qupath.imagej.tools.IJTools
import java.util.regex.Matcher
import java.util.regex.Pattern

import qupath.lib.roi.GeometryTools;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.index.strtree.STRtree;


def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()



def directoryPath = '/home/mariereinbigler/Téléchargements/wetransfer_data-zip_2022-01-07_2150/tiles/S41L2-Scene-11-PSODc2/'
println(directoryPath)

File folder = new File(directoryPath);
File[] listOfFiles = folder.listFiles();


for (File elt: listOfFiles) {

    coord = elt.getName().split("\\[")[1]
    coord = coord.split("\\]")[0]
    coord = coord.split("\\=")
    x = new Integer(coord[1].split("\\,")[0])
    y = new Integer(coord[2].split("\\,")[0])
    w = new Integer(coord[3].split("\\,")[0]) 
    h = new Integer(coord[4].split("\\,")[0])
    
    
    def roi = new RectangleROI(x, y, w, h)
    def annotation = new PathAnnotationObject(roi, PathClassFactory.getPathClass("Region"))
    imageData.getHierarchy().addPathObject(annotation, true)
}

/**
def roi = new RectangleROI(1168, 1744, 712, 712)

// Create & new annotation & add it to the object hierarchy
def annotation = new PathAnnotationObject(roi, PathClassFactory.getPathClass("Region"))
imageData.getHierarchy().addPathObject(annotation, true)
**/
