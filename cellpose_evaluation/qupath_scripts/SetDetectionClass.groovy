
import static qupath.lib.gui.scripting.QPEx.*

def verite = getPathClass('VeriteTerrain')

selected = getAnnotationObjects().findAll{it.getPathClass() == null}


for (def detection in selected){
    detection.setPathClass(verite)
}

