//QP 0.2.3 compliant
//Target a directory of PNG exports from CellPose and import those masks as detection objects into QuPath.

import static qupath.lib.gui.scripting.QPEx.*
import qupath.lib.common.GeneralTools

def directoryPath = '/home/mariereinbigler/These/CellposeArticle/final/test_output/new_model/model105112-70/' // TO CHANGE



print(directoryPath)

clearAllObjects()
double downsample = 1 // TO CHANGE (if needed)
ImagePlane plane = ImagePlane.getDefaultPlane()

File folder = new File(directoryPath);
File[] listOfFiles = folder.listFiles();

print(listOfFiles)

currentImport = listOfFiles.find{GeneralTools.getNameWithoutExtension(it.getPath()).contains(GeneralTools.getNameWithoutExtension(getProjectEntry().getImageName())) &&  it.toString().contains(".png")}

print(currentImport)

path = currentImport.getPath()
    /*if (!path.endsWith(".png"))
        return*/
def imp = IJ.openImage(path)


int n = imp.getStatistics().max as int
if (n == 0) {
    print 'No objects found!'
    return
}
def ip = imp.getProcessor()
    
    if (ip instanceof ColorProcessor) {
        throw new IllegalArgumentException("RGB images are not supported!")
    }
def roisIJ = RoiLabeling.labelsToConnectedROIs(ip, n)

    def rois = roisIJ.collect {
        if (it == null)
            return
        return IJTools.convertToROI(it, 0, 0, downsample, plane);
    }
    rois = rois.findAll{null != it}
    
    // Convert QuPath ROIs to objects
    def pathObjects = rois.collect {
        return PathObjects.createDetectionObject(it)
    }
    addObjects(pathObjects)
    def detections = getDetectionObjects()
    def newAnnotations = detections.collect {
        return PathObjects.createAnnotationObject(it.getROI(), it.getPathClass())
    }
removeObjects(detections, true)
addObjects(newAnnotations)
    
    
//resolveHierarchy()
print "Import completed"

import ij.gui.Wand
import qupath.lib.objects.PathObjects
import qupath.lib.regions.ImagePlane
import ij.IJ
import ij.process.ColorProcessor
import qupath.imagej.processing.RoiLabeling
import qupath.imagej.tools.IJTools
import java.util.regex.Matcher
import java.util.regex.Pattern