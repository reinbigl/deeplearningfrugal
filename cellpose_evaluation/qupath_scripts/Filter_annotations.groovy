
// files of the myofibers' rois "_cp_outlines.txt" must be in the following folder :
// Base Directory containing the "_cp_outlines.txt" files - change manually

import static qupath.lib.gui.scripting.QPEx.*
import qupath.lib.objects.PathObjects
import qupath.lib.regions.ImagePlane

dir = "/home/mariereinbigler/These/Images/Masks/new_test/outlines/"

// Get the name of the current image file (.czi)


// Build the name of the file containing the rois from Cellpose

/**
def imageData = getCurrentImageData()

def rois_name = GeneralTools.getNameWithoutExtension(imageData.getServer().getMetadata().getName()) + ".tiff_cp_outlines.txt"
print(rois_name)

path = dir + rois_name

String fileContents = new File(path).text

clearAllObjects()

**/

//Remove prediction artefacts using object classifier
/**
toChange = getAnnotationObjects()
//toChange = getDetectionObjects()
newObjects = []
toChange.each{
    roi = it.getROI()
    annotation = PathObjects.createDetectionObject(roi, it.getPathClass())
    newObjects.add(annotation)
}

// Actually add the objects
addObjects(newObjects)
//Comment this line out if you want to keep the original objects
removeObjects(toChange,true)

resolveHierarchy()

def cells = getDetectionObjects()

selectDetections();

addShapeMeasurements("AREA", "LENGTH", "CIRCULARITY", "SOLIDITY", "MAX_DIAMETER", "MIN_DIAMETER", "NUCLEUS_CELL_RATIO")
runObjectClassifier("residual_removal")
resetSelection();


removal = getDetectionObjects().findAll{it.getPathClass().toString().contains("Other")}
removeObjects(removal, true)

resetDetectionClassifications()


selectDetections();

runObjectClassifier("test_remove_nuclei")
resetSelection();


removal = getDetectionObjects().findAll{it.getPathClass().toString().contains("Other")}
removeObjects(removal, true)

resetDetectionClassifications()


toChange = getDetectionObjects()
print(toChange)
newObjects = []
toChange.each{
    roi = it.getROI()
    annotation = PathObjects.createAnnotationObject(roi, it.getPathClass())
    newObjects.add(annotation)
}

// Actually add the objects
addObjects(newObjects)
//Comment this line out if you want to keep the original objects
removeObjects(toChange,true)

resolveHierarchy()
print("Done!")


int z = 0
int t = 0
def plane = ImagePlane.getPlane(z, t)
/**
def lines = fileContents.split( '\n' )
print(lines.size())

def annotationsList = []
for(def li=0;li<lines.size();li++){
    if(li%100==0) print(li+" ")
    firstLine = lines[li];
    myValues = firstLine.split(",")
    def xCoords = []
    def yCoords = []
    for (def i=0;i<myValues.size();i=i+2){
        xCoords[i/2] = myValues[i]
        yCoords[i/2] = myValues[i+1]
    }
   def roi = ROIs.createPolygonROI(xCoords as double[], yCoords as double[], plane)
   def annotation = PathObjects.createAnnotationObject(roi)
    annotationsList << annotation
}
addObjects(annotationsList)
**/




//Filter annotation with pixel classifier
def annotation_objects = getAnnotationObjects()

int seuil = 70
double downsample = 1.0
def server = getCurrentServer()

import qupath.lib.gui.measure.ObservableMeasurementTableData

selectAnnotations();
addPixelClassifierMeasurements("Classifier_improved_V16", "Classifier_improved_V16")

def toBeRemoved = []
def observable = new ObservableMeasurementTableData();

observable.setImageData(getCurrentImageData(),  annotation_objects);

for (annotation in annotation_objects){
    measurement = observable.getNumericValue(annotation, "Classifier_improved_V16: Cell %")
    if (measurement < seuil) {
        toBeRemoved << annotation   
  }
   
}

removeObjects(toBeRemoved, true)



